biblatex-ownbib
===============


Bibliography style for my own publications list
-----------------------------------------------
- Based on `numeric-comp`
- Stating author, editor  and translator as LASTNAME, FIRSTNAME
- Always print full names
- Seperate authors with semicolon
- State up to 100 authors (e.g. all); above 'et al.'
- Sorting: year, name, title
- Ragged bibliography
- End list of authors with colon
- Order: pupblisher, location, date


Systems
=======
inedependent

Tested with Tex Live 2020 in Ubuntu 18.04 LTS with tubaf-base + scrartcl, scrbook, moderncv, ...


Licence
=======
The `ownbib` style is licenced under the terms of LPPL 1.3.
